// Soal 1
console.log('Soal 1')
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    // Code di sini
    let object = {};
    if (arr) {
        for (let i = 0; i < arr.length; ++i) {
            let actorsData = {}
            actorsData['firstName'] = arr[i][0];
            actorsData['lastName'] = arr[i][1];
            actorsData['gender'] = arr[i][2];
            actorsData['age'] = arr[i][3]
            if (!arr[i][3] || arr[i][3] > thisYear) {
                actorsData['age'] = 'Invalid Birth Year'
            }
            object[i] = actorsData
        }
        return console.log(object)
    }
}

var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)

arrayToObject([])

// Soal 2
console.log('Soal 2')

function shoppingTime(memberId, money) {
    // you can only write your code here!
    let daftarHarga = {
        'sepatuStacattu': 1500000,
        'bajuZoro': 500000,
        'bajuHAndN': 250000,
        'sweaterUniklooh': 175000,
        'casingHandphone': 50000
    }
    let harga = [1500000, 500000, 250000, 175000, 50000]
    let object = {}
    let cart = []
    if (!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }
    let totalAllBarang = daftarHarga.sepatuStacattu + daftarHarga.bajuZoro + daftarHarga.bajuHAndN + daftarHarga.sweaterUniklooh + daftarHarga.casingHandphone
    if (money == totalAllBarang) {
        object['memberId'] = memberId
        object['money'] = money
        for (barang in daftarHarga) {
            cart.push(barang)
        }
        object['listPurchased'] = cart
        object['changeMoney'] = money - totalAllBarang
    } else {
        for (let i = 0; i < harga.length; i++) {
            if (money > harga[i]) {
                object['memberId'] = memberId
                object['money'] = money
                object['listPurchased'] = money
                object['changeMoney'] = money - harga[i]
            }
        }
    }
    return object
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3
console.log('Soal 3')

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    let data = []
    let object = {}
    for (let i = 0; i < arrPenumpang.length; i++) {
        let naikDari = arrPenumpang[i][1]
        let tujuan = arrPenumpang[i][2]
        let indeksNaikDari = 0
        let indeksTujuan = 0
        let outputNaikDari = ''
        let outputTujuan = ''
        for (let i = 0; i < rute.length; i++) {
            if (naikDari == rute[i]) {
                indeksNaikDari = i
                outputNaikDari = rute[i]
            }
            if (tujuan == rute[i]) {
                indeksTujuan = i
                outputTujuan = rute[i]
            }
        }
        data.push({
            'penumpang': arrPenumpang[i][0],
            'naikDari': outputNaikDari,
            'naikDari': outputNaikDari,
            'tujuan': outputTujuan,
            'bayar': (indeksTujuan - indeksNaikDari) * 2000
        })
    }
    return data
}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
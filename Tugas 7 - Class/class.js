// Soal 1
class Animal {
    // Code class di sini
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
    get animalName() {
        return this.name
    }
    set animalName(x) {
        this.name = x
    }

}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor(name) {
        super(name)
        this.legs = 2
    }
    yell() {
        return console.log('Auooo')
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        return console.log("hop hop")
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// Soal 2
class Clock {
    // Code di sini
    constructor({
        template
    }) {
        this.template = template
        this.timer
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);

    }

    stop() {
        clearInterval(this.timer);
    }

    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();
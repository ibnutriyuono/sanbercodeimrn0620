// Soal 1

// Looping pertama
var x = 2;
var y = 20;
console.log(`LOOPING PERTAMA`)
while (x <= 20) {
    if (x % 2 == 0)
        console.log(`${x} - I Love coding`);
    x++;
}
// Looping kedua
console.log(`LOOPING KEDUA`)
while (y) {
    if (y % 2 == 0)
        console.log(`${y} - I will become a mobile developer`);
    y--;
}

// Soal 2
for (let i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 != 0) {
        console.log(`${i} - I Love Coding`)
    } else if (i % 2 == 0) {
        console.log(`${i} - Berkualitas`)
    } else if (i % 2 != 0) {
        console.log(`${i} - Santai`)
    }
}

// Soal 3
let soal3 = "";
for (let i = 1; i <= 4; i++) {
    for (let j = 1; j <= 8; j++) {
        if (i >= j) {
            soal3 = soal3.concat("#");
        } else {
            soal3 = soal3.concat("#");
        }
    }
    soal3 = soal3.concat("\n")
}

console.log(soal3)

// Soal 4
let soal4 = "";
for (let i = 1; i <= 7; i++) {
    for (let j = 1; j <= 7; j++) {
        if (i >= j) {
            soal4 = soal4.concat("#");
        }
    }
    soal4 = soal4.concat("\n")
}

console.log(soal4)

// Soal 5
let soal5 = "";
for (let i = 1; i <= 8; i++) {
    for (let j = 1; j <= 8; j++) {
        if (0 == (i + j) % 2) {
            soal5 = soal5.concat(` `);
        } else {
            soal5 = soal5.concat(`#`);
        }
    }
    soal5 = soal5.concat("\n")
}

console.log(soal5)
// Soal 1
const teriak = () => {
    return `Halo Sanbers`
}

console.log(teriak())

// Soal 2
var num1 = 12
var num2 = 4

const kalikan = (num1, num2) => {
    return num1 * num2
}

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 30 

//  Soal 3
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

const introduce = (name, age, address) => {
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`
}

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import sanberLogo from "../../../assets/logo.png";
import Constants from "expo-constants";

class RegisterScreen extends Component {
  render() {
    return (
      <SafeAreaView style={styles.safeContainer}>
        <ScrollView style={styles.scrollView}>
          <View style={styles.container}>
            <View style={styles.loginView}>
              <StatusBar />
              <Image source={sanberLogo} style={styles.sanberLogo} />
              <Text style={styles.registerText}>Register</Text>
              <Text style={styles.label}>Username</Text>
              <TextInput
                placeholderColor="#c4c3cb"
                style={styles.registerTextInput}
              />
              <Text style={styles.label}>Email</Text>
              <TextInput
                placeholderColor="#c4c3cb"
                style={styles.registerTextInput}
              />
              <Text style={styles.label}>Password</Text>
              <TextInput
                placeholderColor="#c4c3cb"
                style={styles.registerTextInput}
              />
              <Text style={styles.label}>Ulangi Password</Text>
              <TextInput
                placeholderColor="#c4c3cb"
                style={styles.registerTextInput}
              />
              <TouchableOpacity>
                <Text style={styles.daftarButton}>Daftar</Text>
              </TouchableOpacity>
              <Text style={styles.atauText}>atau</Text>
              <TouchableOpacity>
                <Text style={styles.masukButton}>Masuk ?</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeContainer: { flex: 1, marginTop: Constants.statusBarHeight },
  scrollView: { flex: 1 },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  loginView: {
    flex: 1,
    flexDirection: "column",
  },
  sanberLogo: {
    marginTop: 63,
    marginHorizontal: 20,
    marginBottom: 70,
  },
  registerText: {
    textAlign: "center",
    fontSize: 24,
    color: "#003366",
    marginBottom: 40,
  },
  inputText: {
    height: 50,
  },
  registerTextInput: {
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#003366",
    backgroundColor: "#FFFFFF",
    paddingLeft: 10,
    marginHorizontal: 41,
    marginTop: 4,
    marginBottom: 16,
  },
  label: {
    marginLeft: 41,
    fontSize: 16,
    color: "#003366",
  },
  daftarButton: {
    backgroundColor: "#003366",
    color: "#fff",
    textAlign: "center",
    fontSize: 24,
    marginHorizontal: 118,
    paddingVertical: 20,
    paddingHorizontal: 36,
    marginTop: 40,
    marginBottom: 16,
    borderRadius: 16,
  },
  atauText: {
    color: "#3EC6FF",
    fontSize: 24,
    marginHorizontal: 188,
    marginBottom: 22,
  },
  masukButton: {
    backgroundColor: "#3EC6FF",
    color: "#fff",
    fontSize: 24,
    textAlign: "center",
    marginHorizontal: 118,
    paddingVertical: 20,
    paddingHorizontal: 36,
    marginTop: 40,
    marginBottom: 16,
    borderRadius: 16,
  },
});

export default RegisterScreen;

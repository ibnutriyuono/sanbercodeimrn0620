import React, { Component } from "react";
import { StyleSheet } from "react-native";

import Main from "./components/Main";
import SkillScreen from "./SkillScreen";

export default class App extends Component {
  render() {
    return (
      // <Main />
      <SkillScreen />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    padding: 12,
    justifyContent: "center",
  },
});

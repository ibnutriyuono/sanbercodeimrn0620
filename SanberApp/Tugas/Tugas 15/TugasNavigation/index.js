import React from "react";
import { NavigationContainer, BaseRouter } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Ionicons } from "react-native-vector-icons/Ionicons";

import AboutScreen from "./AboutScreen";
import AddScreen from "./AddScreen";
import { LoginScreen } from "./LoginScreen";
import ProjectScreen from "./ProjectScreen";
import SkillScreen from "./SkillScreen";

const AuthStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="AboutScreen" component={AboutScreen} />
    <Drawer.Screen
      name="Tabs"
      component={TabsScreen}
      options={{ title: "Tabs" }}
    />
  </Drawer.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator
    tabBarOptions={{
      activeTintColor: "#003366",
      inactiveTintColor: "gray",
    }}
  >
    <Tabs.Screen
      name="SkillScreen"
      component={SkillScreen}
      options={{ title: "Skill" }}
    />
    <Tabs.Screen
      name="ProjectScreen"
      component={ProjectScreen}
      options={{ title: "Project" }}
    />
    <Tabs.Screen
      name="AddScreen"
      component={AddScreen}
      options={{ title: "Add" }}
    />
  </Tabs.Navigator>
);

export default () => (
  <NavigationContainer>
    <AuthStack.Navigator>
      <AuthStack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <AuthStack.Screen
        name="AboutScreen"
        component={DrawerScreen}
        options={{ headerShown: false }}
      />
    </AuthStack.Navigator>
  </NavigationContainer>
);

import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

import SkillItem from "./SkillItem";
import data from "./skillData.json";
import sanberLogo from "../../../assets/logo.png";

export default class SkillScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image source={sanberLogo} style={{ height: 60, width: 170 }} />
        </View>
        <View style={styles.account}>
          <Icon name="account-circle" size={40} style={{ color: "#3EC6FF" }} />
          <View style={{ justifyContent: "center" }}>
            <Text style={{ fontSize: 10 }}>Hai,</Text>
            <Text style={styles.textName}>Muhamad Ibnu Tri Yuono</Text>
          </View>
        </View>
        <Text style={styles.skill}>SKILL</Text>
        <View style={styles.tab}>
          <TouchableOpacity style={styles.tabBg}>
            <Text style={styles.tabSkill}>Library / Framework</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabBg}>
            <Text style={styles.tabSkill}>Bahasa Pemrograman</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.tabBg}>
            <Text style={styles.tabSkill}>Teknologi</Text>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
            data={data.items}
            renderItem={(skill) => <SkillItem skill={skill.item} />}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    padding: 12,
  },
  logo: {
    marginVertical: 5,
    alignItems: "flex-end",
  },
  account: {
    flexDirection: "row",
  },
  textName: {
    color: "#003366",
    fontWeight: "bold",
  },
  skill: {
    fontSize: 32,
    borderBottomWidth: 2,
    borderBottomColor: "#3EC6FF",
    color: "#003366",
  },
  tab: {
    marginVertical: 5,
    flexDirection: "row",
  },
  tabBg: {
    marginVertical: 7,
    flexDirection: "column",
    alignItems: "center",
    color: "white",
    borderRadius: 3,
    backgroundColor: "#3EC6FF",
    padding: 5,
    marginRight: 5,
  },
  tabSkill: {
    color: "#003366",
    backgroundColor: "#3EC6FF",
  },
});

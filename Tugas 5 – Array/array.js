// Soal 1
console.log('Soal 1')
const range = (startNum, finishNum) => {
    let array = []
    if (startNum < finishNum) {
        for (startNum; startNum <= finishNum; startNum++) {
            array.push(startNum)
        }
    } else if (startNum > finishNum) {
        for (startNum; startNum >= finishNum; startNum--) {
            array.push(startNum)
        }
    } else {
        return -1
    }
    return array
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2
console.log('Soal 2')
const rangeWithStep = (startNum, finishNum, step) => {
    let array = []
    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            array.push(i)
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            array.push(i)
        }
    } else {
        return -1
    }
    return array
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3
console.log('Soal 3')
const sum = (startNum, finishNum, step = 1) => {
    let array = []
    let total = 0

    if (startNum < finishNum) {
        for (let i = startNum; i <= finishNum; i += step) {
            array.push(i)
        }
        for (let i in array) {
            total += array[i]
        }
    } else if (startNum > finishNum) {
        for (let i = startNum; i >= finishNum; i -= step) {
            array.push(i)
        }
        for (let i in array) {
            total += array[i]
        }
    } else if (!finishNum && startNum) {
        return startNum
    } else {
        return 0
    }
    return total
}
console.log(sum(1, 12, 1)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0  

// Soal 4
console.log('Soal 4')
let input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
// cara 1
const dataHandling = (inputData) => {
    for (i = 0; i < inputData.length; i++) {
        console.log(
            "Nomor ID: " + inputData[i][0] + "\n" +
            "Nama Lengkap: " + inputData[i][1] + "\n" +
            "TTL: " + inputData[i][2] + " " + inputData[i][3] + "\n" +
            "Hobi: " + inputData[i][4] + "\n"
        );
    }
}

dataHandling(input)

// Soal 5
console.log('Soal 5')

const balikKata = (string) => {
    var stringOutput = '';
    for (var i = string.length - 1; i >= 0; i--)
        stringOutput += string[i];
    return stringOutput;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6
console.log('Soal 6')
let input2 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]

const dataHandling2 = (inp) => {
    inp.splice(1, 1, "Roman Alamsyah Elsharawy");
    inp.splice(2, 1, "Provinsi Bandar Lampung");
    inp.splice(4, 1, "Pria", "SMA Internasional Metro");

    split = inp[3].toString().split('/');
    bulan = split[1];
    switch (parseInt(bulan)) {
        case 1:
            console.log("Januari");
            break;
        case 2:
            console.log("Februari");
            break;
        case 3:
            console.log("Maret");
            break;
        case 4:
            console.log("April");
            break;
        case 5:
            console.log("Mei");
            break;
        case 6:
            console.log("Juni");
            break;
        case 7:
            console.log("Juli");
            break;
        case 8:
            console.log("Agustus");
            break;
        case 9:
            console.log("September");
            break;
        case 10:
            console.log("Oktober");
            break;
        case 11:
            console.log("November");
            break;
        case 12:
            console.log("Desember");
            break;
        default:
            console.log('Bulan tidak valid!!');
    }

    join = split.join("-");
    splitDesc = split.sort(function (value1, value2) {
        return value2 - value1
    });

    console.log(splitDesc);
    console.log(join);

    nama = inp[1].slice(0, 14)
    console.log(nama);
}

dataHandling2(input);
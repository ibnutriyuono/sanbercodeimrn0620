// Soal 1
var nama = "John"
var peran = ""

if (nama == 'John' && peran == '') {
    //Output untuk Input nama = 'John' dan peran = ''
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else if (nama == 'Jane' && peran == 'Penyihir') {
    //Output untuk Input nama = 'Jane' dan peran 'Penyihir'   
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == '') {
    //Output untuk Input nama = 'Jenita' dan peran 'Guard'
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
    //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Nama harus diisi!")
}

// Soal 2
var hari = 21;
var bulan = 1;
var tahun = 1945;

switch (bulan) {
    case 1: {
        bulan = "Januari"
        break;
    }
    case 2: {
        bulan = "Februari"
        break;
    }
    case 3: {
        bulan = "Maret"
        break;
    }
    case 4: {
        bulan = "April"
        break;
    }
    case 5: {
        bulan = "Mei"
        break;
    }
    case 6: {
        bulan = "Juni"
        break;
    }
    case 7: {
        bulan = "Juli"
        break;
    }
    case 8: {
        bulan = "Agustus"
        break;
    }
    case 9: {
        bulan = "September"
        break;
    }
    case 10: {
        bulan = "Oktober"
        break;
    }
    case 11: {
        bulan = "November"
        break;
    }
    case 12: {
        bulan = "Desember"
        break;
    }
    default: {
        console.log('Data bulan tidak valid');
    }
}

console.log(`${hari} ${bulan} ${tahun}`)
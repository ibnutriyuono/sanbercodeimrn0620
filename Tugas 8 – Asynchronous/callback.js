// di callback.js
function readBooks(time, book, callback) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function () {
        let sisaWaktu = 0
        if (time > book.timeSpent) {
            sisaWaktu = time - book.timeSpent
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) //menjalankan function callback
        } else {
            console.log('waktu saya habis')
            callback(time)
        }
    }, book.timeSpent)
}

module.exports = readBooks

// function periksaDokter(nomerAntri, callback) {
//     if (nomerAntri > 50) {
//         callback(false)
//     } else if (nomerAntri < 10) {
//         callback(true)
//     }
// }
// periksaDokter(65, function (check) {
//     if (check) {
//         console.log("sebentar lagi giliran saya")
//     } else {
//         console.log("saya jalan-jalan dulu")
//     }
// })